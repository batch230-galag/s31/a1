

const http = require("http");
let port = 3000;

const server = http.createServer((request, response) => {
    if(request.url == '/login') {
        response.writeHead(200, {'Content-type': 'text/plain'});
        response.end('Welcome to the login page');
    } else {
        response.writeHead(404, {'Content-Type': 'text/plain'});
        response.end("I'm sorry the page your are looking for cannot be found.");
    }


    
});

server.listen(port);

console.log('Server now accessible at localhost ' + port);